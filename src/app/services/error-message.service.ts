import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ErrorMessageService {

  showControlValidationError(form: FormGroup, controlName: string): boolean {
    return form.get(controlName).touched && form.get(controlName).invalid;
  }

  getErrorMessage(form: FormGroup, controlName: string) {
    if (form.get(controlName).hasError('required')) {
      return 'This field is required.';
    }

    if (form.get(controlName).hasError('maxlength')) {
      return `Max. available length is ${form.get(controlName).errors.maxlength.requiredLength}.`;
    }

    if (form.get(controlName).hasError('minlength')) {
      return `Min. available length is ${form.get(controlName).errors.minlength.requiredLength}.`;
    }

    if (form.get(controlName).hasError('validateEmail')) {
      return 'Invalid email.';
    }
  }
}

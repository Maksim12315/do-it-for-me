import { Component, OnInit } from '@angular/core';
import { AuthService } from '@services/auth.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { validateEmail } from '@shared/helpers/validators';
import { ErrorMessageService } from '@services/error-message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;

  // email = '03email@winemail.net';
  // email2 = 'bigYolo@winemail.net';
  // email3 = 'test3@winemail.net';
  // email4 = 'test4@winemail.net';
  // email5 = 'test5@winemail.net';
  // password = '111111';
  // displayName = 'John Sina';

  // password2 = '111111';
  // displayName2 = 'Andrew Jackson';
  // displayName3 = 'Peter Parker';
  // displayName4 = 'Ivan Ivanov';
  // displayName5 = 'Some Guy';

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
    public errorMessageService: ErrorMessageService
  ) {}

  ngOnInit() {
    this.initForm();
  }


  loginGoogle() {

  }

  private initForm() {
    this.loginForm  = this.formBuilder.group({
      email: [null, [
        Validators.required,
        Validators.maxLength(40),
        Validators.minLength(6),
        validateEmail
      ]],
      password: [null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ]]
    });
  }

}

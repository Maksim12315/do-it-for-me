import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from '@pages/login/login.page';
import { SharedModule } from '@app/shared/shared.module';
import { RegisterComponent } from '@pages/login/register/register.component';


@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    LoginPageRoutingModule,
    SharedModule
  ],
  declarations: [
    LoginPage,
    RegisterComponent
  ]
})
export class LoginPageModule {}

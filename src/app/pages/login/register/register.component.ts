import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validateEmail } from '@app/shared/helpers/validators';
import { ErrorMessageService } from '@app/services/error-message.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  registrationForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public errorMessageService: ErrorMessageService
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.registrationForm  = this.formBuilder.group({
      email: [null, [
        Validators.required,
        Validators.maxLength(40),
        Validators.minLength(6),
        validateEmail
      ]],
      password: [null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ]]
    });
  }

}

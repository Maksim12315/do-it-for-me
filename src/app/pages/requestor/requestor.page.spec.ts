import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RequestorPage } from './requestor.page';

describe('RequestorPage', () => {
  let component: RequestorPage;
  let fixture: ComponentFixture<RequestorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RequestorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

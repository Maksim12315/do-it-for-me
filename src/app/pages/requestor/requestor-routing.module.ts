import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RequestorPage } from './requestor.page';

const routes: Routes = [
  {
    path: '',
    component: RequestorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RequestorPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RequestorPageRoutingModule } from './requestor-routing.module';

import { RequestorPage } from './requestor.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RequestorPageRoutingModule
  ],
  declarations: [RequestorPage]
})
export class RequestorPageModule {}

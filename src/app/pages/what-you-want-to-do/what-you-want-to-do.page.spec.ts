import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WhatYouWantToDoPage } from './what-you-want-to-do.page';

describe('WhatYouWantToDoPage', () => {
  let component: WhatYouWantToDoPage;
  let fixture: ComponentFixture<WhatYouWantToDoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatYouWantToDoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WhatYouWantToDoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

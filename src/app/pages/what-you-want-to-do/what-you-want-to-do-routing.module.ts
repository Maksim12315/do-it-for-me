import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WhatYouWantToDoPage } from './what-you-want-to-do.page';

const routes: Routes = [
  {
    path: '',
    component: WhatYouWantToDoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WhatYouWantToDoPageRoutingModule {}

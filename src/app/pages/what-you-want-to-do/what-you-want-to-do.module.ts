import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WhatYouWantToDoPageRoutingModule } from './what-you-want-to-do-routing.module';

import { WhatYouWantToDoPage } from './what-you-want-to-do.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WhatYouWantToDoPageRoutingModule
  ],
  declarations: [WhatYouWantToDoPage]
})
export class WhatYouWantToDoPageModule {}

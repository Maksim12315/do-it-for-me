export interface AuctionInstance {
  key: string;
  owner: string;
  displayName: string;
  bid: number;
  requirement: string;
  ask: number;
  deal: string;
  type: string;
  lastCall: number;
  status?: string;
}

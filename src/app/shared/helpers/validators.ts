import { FormControl, AbstractControl } from '@angular/forms';

export function validateEmail(control: AbstractControl) {
  // tslint:disable-next-line:max-line-length
  const emailRegexp =  /^\s*(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))\s*$/;

  return emailRegexp.test(control.value) ? null : {
    validateEmail: {
      valid: false
    }
  };
}

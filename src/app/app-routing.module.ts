import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./pages/what-you-want-to-do/what-you-want-to-do.module').then(
        m => m.WhatYouWantToDoPageModule
      )
  },
  {
    path: 'creator',
    loadChildren: () =>
      import('./pages/creator/creator.module').then(m => m.CreatorPageModule)
  },
  {
    path: 'requestor',
    loadChildren: () =>
      import('./pages/requestor/requestor.module').then(
        m => m.RequestorPageModule
      )
  },
  {
    path: '**',
    loadChildren: () =>
      import('./pages/not-found/not-found.module').then(
        m => m.NotFoundPageModule
      )
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
